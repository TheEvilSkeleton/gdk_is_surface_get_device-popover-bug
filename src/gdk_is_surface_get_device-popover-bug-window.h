/* gdk_is_surface_get_device-popover-bug-window.h
 *
 * Copyright 2024 Unknown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define GDK_IS_SURFACE_GET_DEVICE_POPOVER_BUG_TYPE_WINDOW (gdk_is_surface_get_device_popover_bug_window_get_type())

G_DECLARE_FINAL_TYPE (GdkIsSurfaceGetDevicePopoverBugWindow, gdk_is_surface_get_device_popover_bug_window, GDK_IS_SURFACE_GET_DEVICE_POPOVER_BUG, WINDOW, AdwApplicationWindow)

G_END_DECLS
